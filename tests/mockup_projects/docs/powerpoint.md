---
marp: true
theme: uncover
---

# Hello, I'm Marp CLI!

Write and convert your presentation deck with just a plain Markdown!

---

<!-- backgroundColor: beige -->

## Watch and preview

Marp CLI is supported watch mode and preview window.

---

## We've got images

![hello](gif.gif)

---

# <!--fit--> :+1:
