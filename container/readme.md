# Container CI templates

## Monorepo (multiple Dockerfiles)

You can build multiple `Dockerfile`s in your repository, by using the following
steps. We differentiate between a primary image, and secondary and further
images. The primary image will be used by any subsequent jobs that rely on a
built image to perform further actions. For example, the job `python:pytest`
will be using this primary image.

This image is built by the job `image:build`, which is the default job for
building any image when importing our templates.

We provide two examples with a directory tree and a `.gitlab-ci.yml`.

### One Dockerfile in root, and one in a subdirectory

```shell
monorepo_root
├── .gitlab-ci.yml
├── Dockerfile
└── subdir_b
    ├── Dockerfile
    └── content.txt
```

```yaml
---
include:
  - project: just-ci/templates
    file: templates/container.yml
    ref: v6.25.0

image:build:subdir_b:
  extends: .image:build
  variables:
    IMAGE_CONTEXT: subdir_b

grype:subdir_b:
  extends: .grype
  variables:
    IMAGE_NAME: ${CI_REGISTRY_IMAGE}/subdir_b
  needs:
    - image:build:subdir_b # Optional, but may speed up your pipeline
```

### Two Dockerfiles in two subdirectories

```shell
monorepo_subdir
├── .gitlab-ci.yml
├── subdir_a
│   ├── Dockerfile
│   └── content.txt
└── subdir_b
    ├── Dockerfile
    └── content.txt
```

The first `Dockerfile`'s context can be set with just the variable. For every
subsequent `Dockerfile`, you need to create jobs which extend `.kaniko` and
`.grype`. The example below shows how.

```yaml
---
include:
  - project: just-ci/templates
    file: templates/container.yml
    ref: v6.25.0

variables:
  IMAGE_CONTEXT: subdir_a

image:build:subdir_b:
  extends: .image:build
  variables:
    IMAGE_CONTEXT: subdir_b

grype:subdir_b:
  extends: .grype
  variables:
    IMAGE_NAME: ${CI_REGISTRY_IMAGE}/subdir_b
  needs:
    - image:build:subdir_b # Optional, but may speed up your pipeline
```

You can have as many subdirectories and Dockerfiles as you want. They can even
share contexts. That's all up to you.

The setting above would create the images `${CI_REGISTRY_IMAGE}/subdir_a` and
`${CI_REGISTRY_IMAGE}/subdir_b`. No image will be created at
`${CI_REGISTRY_IMAGE}`. You can override that by adding a global variable called
`IMAGE_NAME: ${CI_REGISTRY_IMAGE}`.

## Different builder

You can change the builder by extending the base image template from a different
builder template. For example:

```yaml
---
include:
  - project: just-ci/templates
    file: templates/container.yml
    ref: v6.25.0

.image:build:
  extends: .buildah # All build jobs will now use buildah

image:build:subdir_b:
  extends: .image:build
  variables:
    IMAGE_CONTEXT: subdir_b

grype:subdir_b:
  extends: .grype
  variables:
    IMAGE_NAME: subdir_b
  needs:
    - image:build:subdir_b # Optional, but may speed up your pipeline
```

## Multiple architectures

To build for multiple architectures, you need the Docker builder. The following
example will show how to have a single Dockerfile built for both amd64 and
arm64.

```yaml
---
include:
  - project: just-ci/templates
    file: templates/container.yml
    ref: v6.25.0

.image:build:
  extends: .docker

variables:
  IMAGE_ARCHITECTURES: linux/arm64,linux/amd64 # Comma separated list. For example: "linux/amd64,linux/ppc64le,linux/arm64,linux/s390x".
```

That's all! All platforms are pushed to the same tag.
