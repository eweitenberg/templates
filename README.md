# Just CI templates v6.25.0

### Maintainers

- Federico Falconieri: @falcorocks
- Ruben ten Hove: @rhtenhove

### Contributors

- Bart Kamphorst: @b-kamphorst

Any questions, problems, suggestions?
[Create an issue](https://gitlab.com/just-ci/templates/-/issues/new)!

This repository contains a collection of modular GitLab CI jobs, pipelines and
templates.

## Structure

- **[Jobs](https://docs.gitlab.com/ee/ci/jobs/)** are individual tasks/tests
  performed over your repository. You can find them in the directory where they
  logically belong to, for example the job for pytest is in `python/pytest.yml`.
  Jobs have the same name of the file they are in (`/` are replaced by `:`) so
  pytest gitlab job name is `python:pytest`. We try to keep the jobs as
  unopinionated/universal as possible. Most of our jobs are for python projects,
  but there are also jobs for c projects and for other automation tasks (badges,
  semantic-versioning, documentation).

- **[Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)** are collections of
  jobs. Jobs can be grouped in
  [stages](https://docs.gitlab.com/ee/ci/pipelines/). Jobs define what to do,
  stages define when (and thus in which order) to run them. Our jobs are all
  configured to always use the default stages `.pre`, `build`, `test`, `deploy`,
  `.post`. We keep our pipelines in `pipelines`. Our pipelines files group
  logically connected jobs. For example `pipelines/python.yml` allows to easily
  import all our python jobs, and `pipelines/docker.yml` allows to easily import
  the Docker jobs and set some other global variables accordingly. Pipelines are
  opinionated, this is where we put workflow rules and make decisions on which
  jobs should run together.

- **Templates** are collections of pipelines. Users should import templates
  rather than dealing with pipelines or jobs directly. They are in the directory
  `templates`. For example the `templates/container/python.yml` provides the
  user with pipelines for python and docker jobs, our default workflow rules and
  project-automation pipeline.

# How to use

In order to use our templates your project will need:

- A `.gitlab-ci.yml` file in the root directory of your repository. This will
  contain references to the jobs, pipelines, and templates you wish to use.
- Optionally a [GitLab runner](https://docs.gitlab.com/ee/ci/runners/)
  configured at project/group level if you don't have shared runners available.

and then for the repository automation jobs to work as intended you will need
to:

- Create a
  [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
  or
  [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
  with **api** scope access. Call it `GL_TOKEN`.
- Store the Access Token in a
  [Gitlab CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
  called `GL_TOKEN`.
  - Set the token to be **Protected** and **Masked**.
- Enable push permission for your
  [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html#configure-a-protected-branch)
  for the fictitious user `GL_TOKEN`.
- Create a `.releaserc` file in the root of your repository. If you don't have
  this file, we provide a
  [default](https://gitlab.com/just-ci/utils/-/raw/main/semantic-release/gitlab-default.json).
- Create a `tbump.toml` file or a `tbump` section in your `pyproject.toml`. See
  [here](https://pypi.org/project/tbump/).

## Choosing a template

You can import our templates in your projects using GitLab remote
[include](https://docs.gitlab.com/ee/ci/yaml/includes.html) functionality. Here
is an example `.gitlab-ci.yml` importing the python template:

```yaml
---
include:
  - remote: https://jobs.just-ci.dev/v6.25.0/templates/python.yml

variables:
  PYVERSION: 3.9-slim
```

The above is a template for Python projects. Here we also add a variable to
specify the python version your project uses. If you omit this variable, all CI
jobs are run using the latest python version. This may introduce unexpected
results, so this variable is highly recommended. You can also omit `-slim` to
use the full debian image, or replace it with `-alpine` if you intend to support
an alpine based image.

Other templates can be found in `templates/`, for example:

- Python projects with a `Dockerfile`: `templates/container/python.yml`. This
  template will use your `Dockerfile` to create an image in which we run tests.
- C projects: `templates/c.yml`.

## Disabling specific jobs

Templates and pipelines may come with jobs you don't want/need to run. We have
carefuly selected our defaults to provide the best code quality possible, but if
you want you can always disable specific jobs with custom rules.

```yaml
---
include:
  - remote: https://jobs.just-ci.dev/v6.25.0/templates/python.yml

variables:
  PYVERSION: 3.9-slim

python:pytest:
  rules:
    - when: never
```

# Pinning your template version

We tag changes in our templates automagically using
[semantic-release](https://semantic-release.gitbook.io/semantic-release/). Every
time a merge request is accepted, a semantic-release job produces a release (and
an associated tag). You can import jobs and pipelines from a specific tag or
branch if you desire. If you do not specify a tag or branch, you will import
what is on the default branch. If your project does not work anymore because
there has been a breaking change in `master`, use an older tag like this:

```yaml
---
include:
  - remote: https://jobs.just-ci.dev/v5.1.0/python/pylint.yml

variables:
  PYVERSION: 3.7-slim
```

# Monorepo (multiple Dockerfiles) or polyrepo (one Dockerfile)

We support both! Read [this](container/readme.md) for more information on how to
configure pipelines for monorepos and polyrepos.

## Contributing

See [`CONTRIBUTING.md`](CONTRIBUTING.md)
